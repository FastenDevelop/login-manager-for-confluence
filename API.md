# API

### Create rule:

**URI:** http://<site-url>/rest/um/1.0/rule/add

**Type query:** POST

**Permissions required:** Access to administration functional

- Parameters:
    - user                                           (Required parameter)
    - rightAndType
        - Type: array
        - Element of array:
            - id - Name object
            - text - Name object
            - type - Type object(**group** -  account-group, **user** -account)
    - restrictionAndType
        - Type: array
        - Element of array:
            - id - Name object
            - text - Name object
            - type - Type object(**group** -  account-group, **user** -account)
    - startDate
    - endDate

Example Json

```javascript
    {
      "user": "admin",
      "rightAndType": [],
      "restrictionAndType": [
        {
          "id": "jira-administrators",
          "text": "jira-administrators",
          "type": "account-group"
        }
      ],
      "startDate": "2019-04-18",
      "endDate": ""
    }
```

### Example:

**CURL:**
```javascript
    curl -u login:pass -H "Content-Type: application/json" -X POST -d '{
      "user": "admin",
      "rightAndType": [],
      "restrictionAndType": [
        {
          "id": "jira-administrators",
          "text": "jira-administrators",
          "type": "account-group"
        }
      ],
      "startDate": "2019-04-18",
      "endDate": ""
    }' http://<site-url>/rest/um/1.0/rule/add
    
```

![Answer](images/addAnswer.png)

---

### Edit rule:

**URI:** http://<site-url>/rest/um/1.0/rule/edit

**Type query:** POST

**Permissions required:** Access to administration functional

- Parameters:
    - id                                               (Required parameter)
    - user                                             (Required parameter)
    - rightAndType
        - Type: array
        - Element of array:
            - id - Name object
            - text - Name object
            - type - Type object(**group** -  account-group, **user** -account)
    - restrictionAndType
        - Type: array
        - Element of array:
            - id - Name object
            - text - Name object
            - type - Type object(**group** -  account-group, **user** -account)
    - startDate
    - endDate

Example Json

```javascript
    {
    	"id": "7",
      "user": "admin",
      "rightAndType": [],
      "restrictionAndType": [
        {
          "id": "jira-administrators",
          "text": "jira-administrators",
          "type": "account-group"
        }
      ],
      "startDate": "2019-04-18",
      "endDate": ""
    }
```

### Example:

**CURL:**

```javascript
    curl -u login:pass -H "Content-Type: application/json" -X POST -d '{
    	"id": "7",
      "user": "admin",
      "rightAndType": [],
      "restrictionAndType": [
        {
          "id": "jira-administrators",
          "text": "jira-administrators",
          "type": "account-group"
        }
      ],
      "startDate": "2019-04-18",
      "endDate": ""
    }' http://<site-url>/rest/um/1.0/rule/edit
```    

![Answer](images/editAnswer.png)

---

### Delete rule

**URI:** http://<site-url>/rest/um/1.0/rule/delete

**Type query:** POST

**Permissions required:** Access to administration functional

- Parameters:
    - id                                                       (Required parameter)

### Example:

**CURL:**

```javascript
    curl -u login:pass -H "Content-Type: application/json" -X POST -d '{
    	"id": "7"
    } http://<site-url>/rest/um/1.0/rule/delete
```

![Answer](images/deleteAnswer.png)

---

### Get a list of those who have access to the functionality of the plugin

**URI:** http://<site-url>/rest/um/1.0/getSettings

**Type query:** GET

**Permissions required:** Access to administration functional

### Example:

**CURL:**

```javascript
    curl -u login:pass -H "Content-Type: application/json" -X GET http://<site-url>/rest/manager/1.0/login/getSettings
```    

Answer from server

```javascript
    [
      {
        "id": "jira-administrators",
        "text": "jira-administrators",
        "type": "account-group"
      },
      {
        "id": "Egorov",
        "text": "Egorov",
        "type": "account"
      },
      {
        "id": "Petrushev",
        "text": "Petrushev",
        "type": "account"
      }
    ]
```  
  
  
Read more [information](https://bitbucket.org/FastenDevelop/login-manager-for-confluence/src/master/README)  